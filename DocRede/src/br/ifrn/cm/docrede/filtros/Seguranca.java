package br.ifrn.cm.docrede.filtros;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.ifrn.cm.docrede.controllers.UsuarioMBean;

@WebFilter("/paginas/*")
public class Seguranca implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		HttpServletResponse resp = (HttpServletResponse) arg1;
		HttpServletRequest req = (HttpServletRequest) arg0;
		
		UsuarioMBean usuarioMBean = (UsuarioMBean) req.getSession().getAttribute("usuarioMBean");
		if(usuarioMBean == null || usuarioMBean.getUsuario() == null || !usuarioMBean.getUsuario().isLogado()) {
			resp.sendRedirect("/DocRede/");
		}
		else arg2.doFilter(req, resp);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
