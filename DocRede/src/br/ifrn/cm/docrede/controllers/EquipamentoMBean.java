package br.ifrn.cm.docrede.controllers;

import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import br.ifrn.cm.docrede.dominio.Computador;
import br.ifrn.cm.docrede.dominio.Equipamento;
import br.ifrn.cm.docrede.dominio.TipoComputador;
import br.ifrn.cm.docrede.dominio.TipoEquipamento;
import br.ifrn.cm.docrede.negocio.ComputadorService;
import br.ifrn.cm.docrede.negocio.EquipamentoService;
import br.ifrn.cm.docrede.negocio.ImpressoraService;
import br.ifrn.cm.docrede.negocio.ProjetorService;
import br.ifrn.cm.docrede.negocio.RamalService;
import br.ifrn.cm.docrede.negocio.ServidorService;
import br.ifrn.cm.docrede.negocio.SwitchService;

@ManagedBean
@SessionScoped
public class EquipamentoMBean {
	
	private Equipamento equipamento;
	private DataModel<Equipamento> equipamentoModel;
	private boolean editarDesativado;
	
	@EJB
	private EquipamentoService equipamentoService;
	@EJB
	private ComputadorService computadorService;
	@EJB
	private ImpressoraService impressoraService;
	@EJB
	private ProjetorService projetorService;
	@EJB
	private RamalService ramalService;
	@EJB
	private ServidorService servidorService;
	@EJB
	private SwitchService switchService;
	
	@ManagedProperty(value="#{computadorMBean}")
	private ComputadorMBean computadorMBean;
	@ManagedProperty(value="#{impressoraMBean}")
	private ImpressoraMBean impressoraMBean;
	@ManagedProperty(value="#{projetorMBean}")
	private ProjetorMBean projetorMBean;
	@ManagedProperty(value="#{ramalMBean}")
	private RamalMBean ramalMBean;
	@ManagedProperty(value="#{servidorMBean}")
	private ServidorMBean servidorMBean;
	@ManagedProperty(value="#{switchMBean}")
	private SwitchMBean switchMBean;
	
	public EquipamentoMBean() {
		equipamento = new Equipamento();
	}

	public Equipamento getEquipamento() {
		return equipamento;
	}

	public void setEquipamento(Equipamento equipamento) {
		this.equipamento = equipamento;
	}

	public DataModel<Equipamento> getEquipamentoModel() {
		return equipamentoModel;
	}

	public void setEquipamentoModel(DataModel<Equipamento> equipamentoModel) {
		this.equipamentoModel = equipamentoModel;
	}
	
	public String contarEquipamentos(String tipo) {
		return equipamentoService.contarEquipamentos(TipoEquipamento.valueOf(tipo)) + "";
	}
	
	public String formEquipamento() {
		equipamento = new Equipamento();
		editarDesativado = false;
		return "/paginas/equipamento/form.jsf";
	}
	
	public String formEquipamento(String tipo) {
		equipamento = new Equipamento();
		equipamento.setTipo(TipoEquipamento.valueOf(tipo));
		editarDesativado = false;
		return "/paginas/equipamento/form.jsf";
	}
	
	public String listarOutros() {
		equipamentoModel = new ListDataModel<Equipamento>(equipamentoService.listarOutros());
		return "/paginas/equipamento/list.jsf";
	}
	
	public List<TipoEquipamento> getTiposEquipamento() {
		return Arrays.asList(TipoEquipamento.ACESSPOINT, TipoEquipamento.CAMERA, TipoEquipamento.ESTABILIZADOR, TipoEquipamento.GATEWAY, TipoEquipamento.MONITOR, TipoEquipamento.NOBREAK, TipoEquipamento.OUTROS, TipoEquipamento.STORAGE);
	}
	
	public String salvar() {
		equipamentoService.salvarOuAtualizar(equipamento);
		return formEquipamento();
	}
	
	public String ver() {
		equipamento = equipamentoModel.getRowData();
		switch (equipamento.getTipo()) {
		case COMPUTADOR:
			Computador aux = computadorService.getByEquipamento(equipamento);
			if(aux.getTipo().equals(TipoComputador.SERVIDOR)){
				servidorMBean.setServidor(servidorService.getByComputador(aux));
				servidorMBean.setEditarDesativado(true);
				return "/paginas/equipamento/computador/servidor/form.jsf";
			} else {
				computadorMBean.setComputador(aux);
				computadorMBean.setEditarDesativado(true);
				return "/paginas/equipamento/computador/form.jsf";
			}
			
		case IMPRESSORA:
			impressoraMBean.setImpressora(impressoraService.getByEquipamento(equipamento));
			impressoraMBean.setEditarDesativado(true);
			return "/paginas/equipamento/impressora/form.jsf";
			
		case PROJETOR:
			projetorMBean.setProjetor(projetorService.getByEquipamento(equipamento));
			projetorMBean.setEditarDesativado(true);
			return "/paginas/equipamento/projetor/form.jsf";
			
		case RAMAL:
			ramalMBean.setRamal(ramalService.getByEquipamento(equipamento));
			ramalMBean.setEditarDesativado(true);
			return "/paginas/equipamento/ramal/form.jsf";
			
		case SWITCH:
			switchMBean.setSwitche(switchService.getByEquipamento(equipamento));
			return "/paginas/equipamento/switch/form.jsf";
			
		default:
			editarDesativado = true;
			return "/paginas/equipamento/form.jsf";
		}
	}
	
	public String deletar() {
		equipamento = equipamentoModel.getRowData();
		switch (equipamento.getTipo()) {
		case COMPUTADOR:
			Computador aux = computadorService.getByEquipamento(equipamento);
			if(aux.getTipo().equals(TipoComputador.SERVIDOR)){
				servidorService.remover(servidorService.getByComputador(aux));
				break;
			} else {
				computadorService.remover(aux);
				break;
			}
			
		case IMPRESSORA:
			impressoraService.remover(impressoraService.getByEquipamento(equipamento));
			break;
			
		case PROJETOR:
			projetorService.remover(projetorService.getByEquipamento(equipamento));
			break;
			
		case RAMAL:
			ramalService.remover(ramalService.getByEquipamento(equipamento));
			break;
			
		case SWITCH:
			switchService.remover(switchService.getByEquipamento(equipamento));
			break;
			
		default:
			equipamentoService.remover(equipamento);
			break;
		}
		return listarTodos();
	}
	
	public String editar() {
		editarDesativado = false;
		return null;
	}
	
	public String listarTodos() {
		equipamentoModel = new ListDataModel<>(equipamentoService.listar());
		return "/paginas/equipamento/list.jsf";
	}

	public ComputadorMBean getComputadorMBean() {
		return computadorMBean;
	}

	public void setComputadorMBean(ComputadorMBean computadorMBean) {
		this.computadorMBean = computadorMBean;
	}

	public ImpressoraMBean getImpressoraMBean() {
		return impressoraMBean;
	}

	public void setImpressoraMBean(ImpressoraMBean impressoraMBean) {
		this.impressoraMBean = impressoraMBean;
	}

	public ProjetorMBean getProjetorMBean() {
		return projetorMBean;
	}

	public void setProjetorMBean(ProjetorMBean projetorMBean) {
		this.projetorMBean = projetorMBean;
	}

	public RamalMBean getRamalMBean() {
		return ramalMBean;
	}

	public void setRamalMBean(RamalMBean ramalMBean) {
		this.ramalMBean = ramalMBean;
	}

	public ServidorMBean getServidorMBean() {
		return servidorMBean;
	}

	public void setServidorMBean(ServidorMBean servidorMBean) {
		this.servidorMBean = servidorMBean;
	}

	public SwitchMBean getSwitchMBean() {
		return switchMBean;
	}

	public void setSwitchMBean(SwitchMBean switchMBean) {
		this.switchMBean = switchMBean;
	}

	public boolean isEditarDesativado() {
		return editarDesativado;
	}

	public void setEditarDesativado(boolean editarDesativado) {
		this.editarDesativado = editarDesativado;
	}
}
