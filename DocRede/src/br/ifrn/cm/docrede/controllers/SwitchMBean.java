package br.ifrn.cm.docrede.controllers;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import br.ifrn.cm.docrede.dominio.Switch;
import br.ifrn.cm.docrede.negocio.SwitchService;

@ManagedBean
@SessionScoped
public class SwitchMBean {
	private Switch switche;
	private DataModel<Switch> switchModel;
	private boolean editarDesativado;
	
	@EJB
	private SwitchService switchService;

	public Switch getSwitche() {
		return this.switche;
	}

	public void setSwitche(Switch switche) {
		this.switche = switche;
	}
	
	public String formSwitch() {
		this.switche = new Switch();
		editarDesativado = false;
		return "/paginas/equipamento/switch/form.jsf";
	}
	
	public String listarSwitch() {
		switchModel = new ListDataModel<>(switchService.listar());
		return "/paginas/equipamento/switch/list.jsf";
	}
	
	public String ver() {
		this.switche = switchModel.getRowData();
		editarDesativado = true;
		return "/paginas/equipamento/switch/form.jsf";
	}
	
	public String deletar() {
		switchService.remover(switchModel.getRowData());
		return "/paginas/equipamento/switch/list.jsf";
	}
	
	public String salvar() {
		switchService.salvarOuAtualizar(this.switche);
		return "/paginas/equipamento/switch/form.jsf";
	}
	
	public String editar() {
		editarDesativado = false;
		return null;
	}

	public DataModel<Switch> getSwitchModel() {
		return switchModel;
	}

	public void setSwitchModel(DataModel<Switch> switchModel) {
		this.switchModel = switchModel;
	}

	public boolean isEditarDesativado() {
		return editarDesativado;
	}

	public void setEditarDesativado(boolean editarDesativado) {
		this.editarDesativado = editarDesativado;
	}
}
