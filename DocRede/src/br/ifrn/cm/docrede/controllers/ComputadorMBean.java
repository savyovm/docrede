package br.ifrn.cm.docrede.controllers;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import br.ifrn.cm.docrede.dominio.Computador;
import br.ifrn.cm.docrede.dominio.TipoComputador;
import br.ifrn.cm.docrede.negocio.ComputadorService;
import br.ifrn.cm.docrede.negocio.ServidorService;

@ManagedBean
@SessionScoped
public class ComputadorMBean {
	
	private Computador computador;
	private boolean isNotebook;
	private DataModel<Computador> computadorModel;
	private boolean editarDesativado;
	
	
	@EJB
	private ComputadorService computadorService;
	
	
	@ManagedProperty(value="#{servidorMBean}")
	private ServidorMBean servidorMBean;
	
	@EJB
	private ServidorService servidorService;
	
	public ComputadorMBean() {
		computador = new Computador();
	}

	public Computador getComputador() {
		return computador;
	}

	public void setComputador(Computador computador) {
		this.computador = computador;
	}

	public DataModel<Computador> getComputadorModel() {
		return computadorModel;
	}

	public void setComputadorModel(DataModel<Computador> computadorModel) {
		this.computadorModel = computadorModel;
	}
	
	public String formComputador() {
		computador = new Computador();
		editarDesativado = false;
		return "/paginas/equipamento/computador/form.jsf";
	}
	
	public String listarComputador() {
		computadorModel = new ListDataModel<Computador>(computadorService.listar());
		return "/paginas/equipamento/computador/list.jsf";
	}

	public boolean isNotebook() {
		if(computador.getTipo() != TipoComputador.NOTEBOOK) {
			isNotebook = false;
		}
		else {
			isNotebook = true;
		}
		return isNotebook;
	}

	public void setNotebook(boolean isNotebook) {
		this.isNotebook = isNotebook;
	}
	
	public String salvar() {
		if(isNotebook) {
			computador.setTipo(TipoComputador.NOTEBOOK);
		}
		else {
			computador.setTipo(TipoComputador.DESKTOP);
		}
		
		computadorService.salvarOuAtualizar(computador);
		return formComputador();
	}
	
	public String ver() {
		computador = computadorModel.getRowData();
		if(computador.getTipo().equals(TipoComputador.SERVIDOR)) {
			servidorMBean.setServidor(servidorService.getByComputador(computador));
			servidorMBean.setEditarDesativado(true);
			return "/paginas/equipamento/computador/servidor/form.jsf";
		}
		else {
			editarDesativado = true;
			return "/paginas/equipamento/computador/form.jsf";
		}
	}
	
	public String deletar() {
		computadorService.remover(computadorModel.getRowData());
		return "/paginas/equipamento/computador/list.jsf";
	}
	
	public String editar() {
		editarDesativado = false;
		return null;
	}

	public ServidorMBean getServidorMBean() {
		return servidorMBean;
	}

	public void setServidorMBean(ServidorMBean servidorMBean) {
		this.servidorMBean = servidorMBean;
	}

	public boolean isEditarDesativado() {
		return editarDesativado;
	}

	public void setEditarDesativado(boolean editarDesativado) {
		this.editarDesativado = editarDesativado;
	}
}
