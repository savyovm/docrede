package br.ifrn.cm.docrede.controllers;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import br.ifrn.cm.docrede.dominio.Impressora;
import br.ifrn.cm.docrede.negocio.ImpressoraService;

@ManagedBean
@SessionScoped
public class ImpressoraMBean {
	private Impressora impressora;
	private DataModel<Impressora> impressoraModel;
	private boolean editarDesativado;
	
	@EJB
	private ImpressoraService impressoraService;
	
	public ImpressoraMBean() {
		impressora = new Impressora();
	}

	public Impressora getImpressora() {
		return impressora;
	}

	public void setImpressora(Impressora impressora) {
		this.impressora = impressora;
	}
	
	public String formImpressora() {
		impressora = new Impressora();
		editarDesativado = false;
		return "/paginas/equipamento/impressora/form.jsf";
	}
	
	public String listarImpressora() {
		impressoraModel = new ListDataModel<Impressora>(impressoraService.listar());
		return "/paginas/equipamento/impressora/list.jsf";
	}
	
	public String salvar() {
		impressoraService.salvarOuAtualizar(impressora);
		return formImpressora();
	}
	
	public String ver() {
		impressora = impressoraModel.getRowData();
		editarDesativado = true;
		return "/paginas/equipamento/impressora/form.jsf";
	}
	
	public String deletar() {
		impressoraService.remover(impressoraModel.getRowData());
		return "/paginas/equipamento/impressora/list.jsf";
	}
	
	public String editar() {
		editarDesativado = false;
		return null;
	}
	
	public DataModel<Impressora> getImpressoraModel() {
		return impressoraModel;
	}

	public void setImpressoraModel(DataModel<Impressora> impressoraModel) {
		this.impressoraModel = impressoraModel;
	}

	public boolean isEditarDesativado() {
		return editarDesativado;
	}

	public void setEditarDesativado(boolean editarDesativado) {
		this.editarDesativado = editarDesativado;
	}
}
