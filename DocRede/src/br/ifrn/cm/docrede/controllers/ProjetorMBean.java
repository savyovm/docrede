package br.ifrn.cm.docrede.controllers;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import br.ifrn.cm.docrede.dominio.Projetor;
import br.ifrn.cm.docrede.negocio.ProjetorService;

@ManagedBean
@SessionScoped
public class ProjetorMBean {
	private Projetor projetor;
	private DataModel<Projetor> projetorModel;
	private boolean editarDesativado;
	
	@EJB
	private ProjetorService projetorService;
	
	public ProjetorMBean() {
		projetor = new Projetor();
	}

	public Projetor getProjetor() {
		return projetor;
	}

	public void setProjetor(Projetor projetor) {
		this.projetor = projetor;
	}
	
	public String formProjetor() {
		projetor = new Projetor();
		editarDesativado = false;
		return "/paginas/equipamento/projetor/form.jsf";
	}
	
	public String listarProjetor() {
		projetorModel = new ListDataModel<Projetor>(projetorService.listar());
		return "/paginas/equipamento/projetor/list.jsf";
	}
	
	public String salvar() {
		projetorService.salvarOuAtualizar(projetor);
		return formProjetor();
	}
	
	public String ver() {
		projetor = projetorModel.getRowData();
		editarDesativado = true;
		return "/paginas/equipamento/projetor/form.jsf";
	}
	
	public String deletar() {
		projetorService.remover(projetorModel.getRowData());
		return "/paginas/equipamento/projetor/list.jsf";
	}
	
	public String editar() {
		editarDesativado = false;
		return null;
	}

	public DataModel<Projetor> getProjetorModel() {
		return projetorModel;
	}

	public void setProjetorModel(DataModel<Projetor> projetorModel) {
		this.projetorModel = projetorModel;
	}

	public boolean isEditarDesativado() {
		return editarDesativado;
	}

	public void setEditarDesativado(boolean editarDesativado) {
		this.editarDesativado = editarDesativado;
	}
}
