package br.ifrn.cm.docrede.controllers;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import br.ifrn.cm.docrede.dominio.Ramal;
import br.ifrn.cm.docrede.negocio.RamalService;

@ManagedBean
@SessionScoped
public class RamalMBean {
	private Ramal ramal;
	private DataModel<Ramal> ramalModel;
	private boolean editarDesativado;
	
	@EJB
	private RamalService ramalService;
	
	public RamalMBean() {
		ramal = new Ramal();
	}

	public Ramal getRamal() {
		return ramal;
	}

	public void setRamal(Ramal ramal) {
		this.ramal = ramal;
	}
	
	public String formRamal() {
		ramal = new Ramal();
		editarDesativado = false;
		return "/paginas/equipamento/ramal/form.jsf";
	}
	
	public String listarRamal() {
		ramalModel = new ListDataModel<Ramal>(ramalService.listar());
		return "/paginas/equipamento/ramal/list.jsf";
	}
	
	public String salvar() {
		ramalService.salvarOuAtualizar(ramal);
		return formRamal();
	}
	
	public String ver() {
		ramal = ramalModel.getRowData();
		editarDesativado = true;
		return "/paginas/equipamento/ramal/form.jsf";
	}
	
	public String deletar() {
		ramalService.remover(ramalModel.getRowData());
		return "/paginas/equipamento/ramal/list.jsf";
	}
	
	public String editar() {
		editarDesativado = false;
		return null;
	}

	public DataModel<Ramal> getRamalModel() {
		return ramalModel;
	}

	public void setRamalModel(DataModel<Ramal> ramalModel) {
		this.ramalModel = ramalModel;
	}

	public boolean isEditarDesativado() {
		return editarDesativado;
	}

	public void setEditarDesativado(boolean editarDesativado) {
		this.editarDesativado = editarDesativado;
	}
}
