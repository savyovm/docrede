package br.ifrn.cm.docrede.controllers;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import br.ifrn.cm.docrede.dominio.Servidor;
import br.ifrn.cm.docrede.negocio.ServidorService;

@ManagedBean
@SessionScoped
public class ServidorMBean {
	private Servidor servidor;
	private DataModel<Servidor> servidorModel;
	private boolean editarDesativado;
	
	@EJB
	private ServidorService servidorService;
	
	public ServidorMBean() {
		servidor = new Servidor();
	}

	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}
	
	public String formServidor() {
		servidor = new Servidor();
		editarDesativado = false;
		return "/paginas/equipamento/computador/servidor/form.jsf";
	}
	
	public String listarServidor() {
		servidorModel = new ListDataModel<Servidor>(servidorService.listar());
		return "/paginas/equipamento/computador/servidor/list.jsf";
	}
	
	public String salvar(){
		servidorService.salvarOuAtualizar(servidor);
		return formServidor();
	}
	
	public String ver() {
		servidor = servidorModel.getRowData();
		editarDesativado = true;
		return "/paginas/equipamento/computador/servidor/form.jsf";
	}
	
	public String deletar() {
		servidorService.remover(servidorModel.getRowData());
		return "/paginas/equipamento/computador/servidor/list.jsf";
	}
	
	public String editar() {
		editarDesativado = false;
		return null;
	}

	public DataModel<Servidor> getServidorModel() {
		return servidorModel;
	}

	public void setServidorModel(DataModel<Servidor> servidorModel) {
		this.servidorModel = servidorModel;
	}

	public boolean isEditarDesativado() {
		return editarDesativado;
	}

	public void setEditarDesativado(boolean editarDesativado) {
		this.editarDesativado = editarDesativado;
	}
}
