package br.ifrn.cm.docrede.controllers;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.ifrn.cm.docrede.dominio.Usuario;
import br.ifrn.cm.docrede.negocio.UsuarioService;

@ManagedBean
@SessionScoped
public class UsuarioMBean {
	private Usuario usuario;
	private String login;
	private String senha;
	
	@EJB
	UsuarioService usuarioService;
	
	public UsuarioMBean() {
		usuario = new Usuario();
		usuario.setLogado(false);
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String entrar() {
		
		try {
			usuario.setLogado(usuarioService.autenticar(login, senha));
			
			if (usuario.isLogado()) {
				login = "";
				senha = "";
				return "/paginas/index.jsf";
			}
			else {
				senha="";
				FacesMessage msg = new FacesMessage("Usu�rio ou senha incorretos");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				FacesContext.getCurrentInstance().addMessage("", msg);
				return null;
			}
		} catch (Exception e) {
			FacesMessage msg = new FacesMessage("Ocorreu um erro inesperado");
			msg.setSeverity(FacesMessage.SEVERITY_FATAL);
			FacesContext.getCurrentInstance().addMessage("Erro!", msg);
			return null;
		}
	}
	
	public String sair() {
		usuario.setLogado(false);
		return "/paginas/sair.jsf";
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}
