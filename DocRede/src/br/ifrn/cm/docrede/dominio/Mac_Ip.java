package br.ifrn.cm.docrede.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Mac_Ip {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_MAC_IP")
	@SequenceGenerator(name="SEQ_MAC_IP", sequenceName="seq_mac_ip", allocationSize=1)
	private int id;
	@ManyToOne
	@JoinColumn(name="id_mac")
	private Mac mac;
	@ManyToOne
	@JoinColumn(name="id_ip")
	private Ip ip;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Mac getMac() {
		return mac;
	}
	public void setMac(Mac mac) {
		this.mac = mac;
	}
	public Ip getIp() {
		return ip;
	}
	public void setIp(Ip ip) {
		this.ip = ip;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mac_Ip other = (Mac_Ip) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
