package br.ifrn.cm.docrede.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Impressora {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_IMPRESSORA")
	@SequenceGenerator(name="SEQ_IMPRESSORA", sequenceName="seq_impressora", allocationSize=1)
	private int id;
	@ManyToOne
	@JoinColumn(name="id_equipamento_impressora")
	private Equipamento equipamento;
	private boolean colorida;
	private boolean ativa;
	private String nome;
	
	public Impressora() {
		equipamento = new Equipamento();
		equipamento.setTipo(TipoEquipamento.IMPRESSORA);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Equipamento getEquipamento() {
		return equipamento;
	}
	public void setEquipamento(Equipamento equipamento) {
		this.equipamento = equipamento;
	}
	public boolean isColorida() {
		return colorida;
	}
	public void setColorida(boolean colorida) {
		this.colorida = colorida;
	}
	public boolean isAtiva() {
		return ativa;
	}
	public void setAtiva(boolean ativa) {
		this.ativa = ativa;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Impressora other = (Impressora) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
