package br.ifrn.cm.docrede.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Ramal {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_RAMAL")
	@SequenceGenerator(name="SEQ_RAMAL", sequenceName="seq_ramal", allocationSize=1)
	private int id;
	@ManyToOne
	@JoinColumn(name="id_equipamento_ramal")
	private Equipamento equipamento;
	private int numero;
	private boolean ativo;
	
	public Ramal() {
		equipamento = new Equipamento();
		equipamento.setTipo(TipoEquipamento.RAMAL);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Equipamento getEquipamento() {
		return equipamento;
	}
	public void setEquipamento(Equipamento equipamento) {
		this.equipamento = equipamento;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ramal other = (Ramal) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
