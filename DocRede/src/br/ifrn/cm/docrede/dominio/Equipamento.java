package br.ifrn.cm.docrede.dominio;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Equipamento {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_EQUIPAMENTO")
	@SequenceGenerator(name="SEQ_EQUIPAMENTO", sequenceName="seq_equipamento", allocationSize=1)
	private int id;
	private int patrimonio;
	private String modelo;
	private String marca;
	private String sala;
	private String predio;
	private Date dataCompra;
	private Date dataSubstituicao;
	private int garantia;
	private String serialNumber;
	@Enumerated(EnumType.STRING)
	private TipoEquipamento tipo;
	
	public Equipamento() {
		tipo = TipoEquipamento.OUTROS;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPatrimonio() {
		return patrimonio;
	}
	public void setPatrimonio(int patrimonio) {
		this.patrimonio = patrimonio;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getSala() {
		return sala;
	}
	public void setSala(String sala) {
		this.sala = sala;
	}
	public String getPredio() {
		return predio;
	}
	public void setPredio(String predio) {
		this.predio = predio;
	}
	public Date getDataCompra() {
		return dataCompra;
	}
	public void setDataCompra(Date data_compra) {
		this.dataCompra = data_compra;
	}
	public Date getDataSubstituicao() {
		return dataSubstituicao;
	}
	public void setDataSubstituicao(Date data_substituicao) {
		this.dataSubstituicao = data_substituicao;
	}
	public int getGarantia() {
		return garantia;
	}
	public void setGarantia(int garantia) {
		this.garantia = garantia;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serial_number) {
		this.serialNumber = serial_number;
	}
	public TipoEquipamento getTipo() {
		return tipo;
	}
	public void setTipo(TipoEquipamento tipo) {
		this.tipo = tipo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Equipamento other = (Equipamento) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
