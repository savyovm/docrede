package br.ifrn.cm.docrede.dominio;

public enum TipoComputador {
	DESKTOP, NOTEBOOK, SERVIDOR;
}
