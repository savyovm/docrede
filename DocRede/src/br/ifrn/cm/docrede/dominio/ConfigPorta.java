package br.ifrn.cm.docrede.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class ConfigPorta {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_CONFIG_PORTA")
	@SequenceGenerator(name="SEQ_CONFIG_PORTA", sequenceName="seq_config_porta", allocationSize=1)
	private int id;
	@ManyToOne
	@JoinColumn(name="id_vlan")
	private Vlan vlan;
	@ManyToOne
	@JoinColumn(name="id_switch")
	private Switch idSwitch;
	private int porta;
	private boolean tagged;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConfigPorta other = (ConfigPorta) obj;
		if (id != other.id)
			return false;
		return true;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Vlan getVlan() {
		return vlan;
	}
	public void setVlan(Vlan vlan) {
		this.vlan = vlan;
	}
	public Switch getIdSwitch() {
		return idSwitch;
	}
	public void setIdSwitch(Switch idSwitch) {
		this.idSwitch = idSwitch;
	}
	public int getPorta() {
		return porta;
	}
	public void setPorta(int porta) {
		this.porta = porta;
	}
	public boolean isTagged() {
		return tagged;
	}
	public void setTagged(boolean tagged) {
		this.tagged = tagged;
	}
	
}
