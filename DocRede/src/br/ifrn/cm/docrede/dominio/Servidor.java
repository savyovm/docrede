package br.ifrn.cm.docrede.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Servidor {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_SERVIDOR")
	@SequenceGenerator(name="SEQ_SERVIDOR", sequenceName="seq_servidor", allocationSize=1)
	private int id;
	@ManyToOne
	@JoinColumn(name="id_computador")
	private Computador computador;
	private boolean virtual;
	private String servicos;
	
	public Servidor() {
		computador = new Computador();
		computador.setTipo(TipoComputador.SERVIDOR);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Computador getComputador() {
		return computador;
	}
	public void setComputador(Computador computador) {
		this.computador = computador;
	}
	public boolean isVirtual() {
		return virtual;
	}
	public void setVirtual(boolean virtual) {
		this.virtual = virtual;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Servidor other = (Servidor) obj;
		if (id != other.id)
			return false;
		return true;
	}
	public String getServicos() {
		return servicos;
	}
	public void setServicos(String servicos) {
		this.servicos = servicos;
	}
	
}
