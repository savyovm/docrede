package br.ifrn.cm.docrede.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Permissao {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_PERMISSAO")
	@SequenceGenerator(name="SEQ_PERMISSAO", sequenceName="seq_permissao", allocationSize=1)
	private int id;
	@ManyToOne
	@JoinColumn(name="id_grupo")
	private Grupo grupo;
	@ManyToOne
	@JoinColumn(name="id_impressora")
	private Impressora impressora;
	private boolean imprimir;
	private boolean gerImpressora;
	private boolean gerDocumentos;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Grupo getGrupo() {
		return grupo;
	}
	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}
	public Impressora getImpressora() {
		return impressora;
	}
	public void setImpressora(Impressora impressora) {
		this.impressora = impressora;
	}
	public boolean isImprimir() {
		return imprimir;
	}
	public void setImprimir(boolean imprimir) {
		this.imprimir = imprimir;
	}
	public boolean isGerImpressora() {
		return gerImpressora;
	}
	public void setGerImpressora(boolean gerImpressora) {
		this.gerImpressora = gerImpressora;
	}
	public boolean isGerDocumentos() {
		return gerDocumentos;
	}
	public void setGerDocumentos(boolean gerDocumentos) {
		this.gerDocumentos = gerDocumentos;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Permissao other = (Permissao) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
