package br.ifrn.cm.docrede.dominio;

public class Usuario {
	private boolean logado;

	public boolean isLogado() {
		return logado;
	}

	public void setLogado(boolean logado) {
		this.logado = logado;
	}
}
