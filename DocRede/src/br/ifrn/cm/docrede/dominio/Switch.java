package br.ifrn.cm.docrede.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Switch {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_SWITCH")
	@SequenceGenerator(name="SEQ_SWITCH", sequenceName="seq_switch", allocationSize=1)
	private int id;
	@ManyToOne
	@JoinColumn(name="id_equipamento_switch")
	private Equipamento equipamento;
	private int portas;
	private int rack;
	private int nome;
	
	public Switch() {
		equipamento = new Equipamento();
		equipamento.setTipo(TipoEquipamento.SWITCH);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Switch other = (Switch) obj;
		if (id != other.id)
			return false;
		return true;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Equipamento getEquipamento() {
		return equipamento;
	}
	public void setEquipamento(Equipamento equipamento) {
		this.equipamento = equipamento;
	}
	public int getPortas() {
		return portas;
	}
	public void setPortas(int portas) {
		this.portas = portas;
	}
	public int getRack() {
		return rack;
	}
	public void setRack(int rack) {
		this.rack = rack;
	}

	public int getNome() {
		return nome;
	}

	public void setNome(int nome) {
		this.nome = nome;
	}
	
}
