package br.ifrn.cm.docrede.dominio;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Computador {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_COMPUTADOR")
	@SequenceGenerator(name="SEQ_COMPUTADOR", sequenceName="seq_computador", allocationSize=1)
	private int id;
	@ManyToOne
	@JoinColumn(name="id_equipamento_computador")
	private Equipamento equipamento;
	private String nome;
	private String sistemaOperacional;
	@Enumerated(EnumType.STRING)
	private TipoComputador tipo;
	
	public Computador() {
		equipamento = new Equipamento();
		equipamento.setTipo(TipoEquipamento.COMPUTADOR);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Computador other = (Computador) obj;
		if (id != other.id)
			return false;
		return true;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Equipamento getEquipamento() {
		return equipamento;
	}
	public void setEquipamento(Equipamento equipamento) {
		this.equipamento = equipamento;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSistemaOperacional() {
		return sistemaOperacional;
	}
	public void setSistemaOperacional(String sistemaOperacional) {
		this.sistemaOperacional = sistemaOperacional;
	}
	public TipoComputador getTipo() {
		return tipo;
	}
	public void setTipo(TipoComputador tipo) {
		this.tipo = tipo;
	}
}
