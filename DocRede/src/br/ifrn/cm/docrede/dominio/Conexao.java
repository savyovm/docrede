package br.ifrn.cm.docrede.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Conexao {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_CONEXAO")
	@SequenceGenerator(name="SEQ_CONEXAO", sequenceName="seq_conexao", allocationSize=1)
	private int id;
	@ManyToOne
	@JoinColumn(name="id_mac_a")
	private Mac macA;
	@ManyToOne
	@JoinColumn(name="id_mac_b")
	private Mac macB;
	private String portaSwA;
	private String portaSwB;
	private int rack;
	private int portaPp;
	private char patchPainel;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Mac getMacA() {
		return macA;
	}
	public void setMacA(Mac macA) {
		this.macA = macA;
	}
	public Mac getMacB() {
		return macB;
	}
	public void setMacB(Mac macB) {
		this.macB = macB;
	}
	public String getPortaSwA() {
		return portaSwA;
	}
	public void setPortaSwA(String portaSwA) {
		this.portaSwA = portaSwA;
	}
	public String getPortaSwB() {
		return portaSwB;
	}
	public void setPortaSwB(String portaSwB) {
		this.portaSwB = portaSwB;
	}
	public int getRack() {
		return rack;
	}
	public void setRack(int rack) {
		this.rack = rack;
	}
	public int getPortaPp() {
		return portaPp;
	}
	public void setPortaPp(int portaPp) {
		this.portaPp = portaPp;
	}
	public char getPatchPainel() {
		return patchPainel;
	}
	public void setPatchPainel(char patchPainel) {
		this.patchPainel = patchPainel;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conexao other = (Conexao) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
