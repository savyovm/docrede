package br.ifrn.cm.docrede.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.ifrn.cm.docrede.dominio.Equipamento;
import br.ifrn.cm.docrede.dominio.Ramal;

@Stateless
public class RamalDao {
	
	@PersistenceContext
	private EntityManager em;
	
	@Inject
	private EquipamentoDao equipamentoDao = new EquipamentoDao();
	
	public Ramal salvarOuAtualizar(Ramal ramal){
		ramal.setEquipamento(equipamentoDao.salvarOuAtualizar(ramal.getEquipamento()));
		if(ramal.getId() == 0) em.persist(ramal);
		else em.merge(ramal);
		
		return ramal;
	}
	
	public void remover(Ramal ramal){
		ramal = em.find(Ramal.class, ramal.getId());
		equipamentoDao.remover(ramal.getEquipamento());
		em.remove(ramal);
	}
	
	@SuppressWarnings("unchecked")
	public List<Ramal> listar() {
		return (List<Ramal>) em.createQuery("select r from Ramal r").getResultList();
	}
	
	public Ramal getRamal(int id) {
		return em.find(Ramal.class, id);
	}
	
	public Ramal getByEquipamento(Equipamento id) {
		Query q = em.createQuery("SELECT r FROM Ramal r WHERE r.equipamento = :equipamento");
		q.setParameter("equipamento", id);
		return (Ramal) q.getSingleResult();
	}
	
	/*protected static List<String> montarParametros(Ramal ramal) {
	*	if(ramal != null) {
	*		List<String> parametros = new ArrayList<String>();
	*		
	*		if(ramal.getNumero() != 0) parametros.add(" r.numero = :numero");
	*		
	*		return parametros;
	*	} else return null;
	}*/
	
	/*protected static List<String> montarParametros(List<Integer> numeros) {
	*	List<String> parametros = new ArrayList<String>();
	*	
	*	if(!numeros.isEmpty()) parametros.add(" r.numero in (:numero)");
	*	
	*	return parametros;
	}*/
}
