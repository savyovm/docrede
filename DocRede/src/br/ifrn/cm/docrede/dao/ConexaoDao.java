package br.ifrn.cm.docrede.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import br.ifrn.cm.docrede.dominio.Conexao;

@Stateless
public class ConexaoDao {
	
	@PersistenceContext
	private EntityManager em;
	
	public Conexao salvarOuAtualizar(Conexao conexao) {
		if(conexao.getId() == 0) em.persist(conexao);
		else em.merge(conexao);
		
		return conexao;
	}
	
	public void remover(Conexao conexao) {
		conexao = em.find(Conexao.class, conexao);
		em.remove(conexao);
	}
	
	@SuppressWarnings("unchecked")
	public List<Conexao> listar() {
		return (List<Conexao>) em.createQuery("select co from Conexao co").getResultList();
	}
	
	public Conexao getConexao(int id) {
		return em.find(Conexao.class, id);
	}
	
	/*public List<Conexao> buscar(Conexao conexao) {
	*	String consulta = "select co from Conexao co";
	*	List<String> parametros = montarParametros(conexao);
	*	
	*	if(conexao.getMacA() != null || conexao.getMacB() != null) {
	*		consulta += " JOIN Mac " + MacDao.PREFIXO + " where " + MacDao.PREFIXO + ".id = co.macA OR " + MacDao.PREFIXO + ".id = co.macB";
	*		
	*		if(conexao.getMacA() != null) {
	*			parametros.addAll(MacDao.montarParametros(conexao.getMacA()));
	*			if(conexao.getMacA().getEquipamento() != null) parametros.addAll(EquipamentoDao.montarParametros(conexao.getMacA().getEquipamento()));
	*		}
	*		
	*		if(conexao.getMacB() != null) {
	*			parametros.addAll(MacDao.montarParametros(conexao.getMacB()));
	*			if(conexao.getMacB().getEquipamento() != null) parametros.addAll(EquipamentoDao.montarParametros(conexao.getMacB().getEquipamento()));
	*		}
	*		
	*		if(conexao.getMacA().getEquipamento() != null || conexao.getMacB().getEquipamento() != null) consulta += " JOIN Equipamento " + EquipamentoDao.PREFIXO + " where " + EquipamentoDao.PREFIXO + ".id = " + MacDao.PREFIXO + ".equipamento";
	*	}
	*	
	*	if(!parametros.isEmpty()) {
	*		consulta += " where" + parametros.get(0);
	*		
	*		for (Iterator<String> iterator = parametros.iterator(); iterator.hasNext();) {
	*			String string = (String) iterator.next();
	*			consulta += " and" + string;
	*		}
	*		
	*		Query q = em.createQuery(consulta);
	*		if(consulta.contains("macA")) q.setParameter("macA", conexao.getMacA());
	*		if(consulta.contains("macB")) q.setParameter("macB", conexao.getMacB());
	*		if(consulta.contains("portaSwA")) q.setParameter("portaSwA", conexao.getPortaSwA());
	*		if(consulta.contains("portaSwB")) q.setParameter("portaSwB", conexao.getPortaSwB());
	*		if(consulta.contains("rack")) q.setParameter("rack", conexao.getRack());
	*		if(consulta.contains("portaPp")) q.setParameter("portaPp", conexao.getPortaPp());
	*		if(consulta.contains("patchPainel")) q.setParameter("patchPainel", conexao.getPatchPainel());
	*		if(consulta.contains("endereco")) q.setParameter("endereco", conexao.getPatchPainel());
	*	}
	*	return null;
	*}*/
	
	/*protected static List<String> montarParametros(Conexao conexao) {
	*	if(conexao != null){
	*		List<String> parametros = new ArrayList<String>();
	*		
	*			boolean macA = conexao.getMacA() != null;
	*			boolean macB = conexao.getMacB() != null;
	*			boolean portaSwA = conexao.getPortaSwA() != null;
	*			boolean portaSwB = conexao.getPortaSwB() != null;
	*			boolean rack = conexao.getRack() != 0;
	*			boolean portaPp = conexao.getPortaPp() != 0;
	*			boolean patchPainel = conexao.getPatchPainel() >= Character.MIN_VALUE;
	*			
	*			if(macA) parametros.add(" co.macA = :macA");
	*			if(macB) parametros.add(" co.macB = :macB");
	*			if(portaSwA) parametros.add(" co.portaSwA like %:portaSwA%");
	*			if(portaSwB) parametros.add(" co.portaSwB like %:portaSwB%");
	*			if(rack) parametros.add(" co.rack like %:rack%");
	*			if(portaPp) parametros.add(" co.portaPp like %:portaPp%");
	*			if(patchPainel) parametros.add(" co.patchPainel like %:patchPainel%");
	*			
	*		return parametros;
	*	}
	*	else return null; 
	}*/
	
	/*protected static List<String> montarParametros(List<Mac> macsA, List<Mac> macsB, List<String> portasSwA, List<String> portasSwB, List<Integer> racks, List<Integer> portasPp, List<Character> patchPainels){
	*	
	*	List<String> parametros = new ArrayList<String>();
	*	
	*	if(!macsA.isEmpty()) parametros.add(" co.macA in (:macA)");
	*	if(!macsB.isEmpty()) parametros.add(" co.macB in (:macB)");
	*	if(!portasSwA.isEmpty()) parametros.add(" co.portaSwA in (:portaSwA)");
	*	if(!portasSwB.isEmpty()) parametros.add(" co.portaSwB in (:portaSwB)");
	*	if(!racks.isEmpty()) parametros.add(" co.rack in (:rack)");
	*	if(!portasPp.isEmpty()) parametros.add(" co.portaPp in (:portaPp)");
	*	if(!patchPainels.isEmpty()) parametros.add(" co.patchPainel in (:patchPainel)");
	*	
	*	return parametros;
	}*/
}
