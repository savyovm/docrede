package br.ifrn.cm.docrede.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import br.ifrn.cm.docrede.dominio.ConfigPorta;


@Stateless
public class ConfigPortaDao {
	
	@PersistenceContext
	private EntityManager em;
	
	public ConfigPorta salvarOuAtualizar(ConfigPorta configPorta){
		if(configPorta.getId() == 0) em.persist(configPorta);
		else em.merge(configPorta);
		
		return configPorta;
	}
	
	public void remover(ConfigPorta configPorta){
		configPorta = em.find(ConfigPorta.class, configPorta.getId());
		em.remove(configPorta);
	}
	
	@SuppressWarnings("unchecked")
	public List<ConfigPorta> listar() {
		return (List<ConfigPorta>) em.createQuery("select cp from ConfigPorta cp").getResultList();
	}
	
	public ConfigPorta getConfigPorta(int id) {		
		return (ConfigPorta) em.find(ConfigPorta.class, id);
	}
	
	/*protected static List<String> montarParametros(ConfigPorta configPorta) {
	*	if(configPorta != null){
	*		
	*		List<String> parametros  = new ArrayList<String>();
	*		
	*		if(configPorta.getVlan() != null) parametros.add(" cp.vlan = :vlan");
	*		if(configPorta.getIdSwitch() != null) parametros.add(" cp.idSwitch = :idSwitch");
	*		if(configPorta.getPorta() != 0) parametros.add(" cp.porta = :porta");
	*		
	*		return parametros;
	*	} else return null;
	}*/
	
	/*protected static List<String> montarParametros(List<Vlan> vlans, List<Switch> switches, List<Integer> portas) {
	*	
	*	List<String> parametros = new ArrayList<String>();
	*	
	*	if(!vlans.isEmpty()) parametros.add(" cp.vlan in (:vlan)");
	*	if(!switches.isEmpty()) parametros.add(" cp.idSwitch in (:idSwitch)");
	*	if(!portas.isEmpty()) parametros.add(" cp.porta in (:porta)");
	*	
	*	return parametros;
	}*/
}
