package br.ifrn.cm.docrede.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.ifrn.cm.docrede.dominio.Computador;
import br.ifrn.cm.docrede.dominio.Equipamento;

@Stateless
public class ComputadorDao {	
	@PersistenceContext
	private EntityManager em;
	
	@Inject
	private EquipamentoDao equipamentoDao = new EquipamentoDao();
	
	public Computador salvarOuAtualizar(Computador computador){
		computador.setEquipamento(equipamentoDao.salvarOuAtualizar(computador.getEquipamento()));
		if(computador.getId() == 0) em.persist(computador);
		else em.merge(computador);
		
		return computador;
	}
	
	public void remover(Computador computador){
		computador = em.find(Computador.class, computador.getId());
		equipamentoDao.remover(computador.getEquipamento());
		em.remove(computador);
	}
	
	@SuppressWarnings("unchecked")
	public List<Computador> listar() {
		return (List<Computador>) em.createQuery("select c from Computador c").getResultList();
	}
	
	public Computador getComputador(int id) {
		return em.find(Computador.class, id);
	}
	
	public Computador getByEquipamento(Equipamento id) {
		Query q = em.createQuery("SELECT c FROM Computador c WHERE c.equipamento = :equipamento");
		q.setParameter("equipamento", id);
		return (Computador) q.getSingleResult();
	}
	
	/*@SuppressWarnings("unchecked")
	*public List<Computador> buscar(Computador computador) {
	*	String consulta = "select c from Computador c";
	*	List<String> parametros = new ArrayList<String>();
	*	
	*	parametros.addAll(montarParametros(computador));
	*	
	*	if(computador.getEquipamento() != null) {
	*		consulta += " JOIN Equipamento " + EquipamentoDao.PREFIXO + " where " + EquipamentoDao.PREFIXO + ".id = c.equipamento";
	*		parametros.addAll(EquipamentoDao.montarParametros(computador.getEquipamento()));
	*	}
	*	
	*	if(!parametros.isEmpty()) {
	*		consulta += " where" + parametros.get(0);
	*		
	*		for (Iterator<String> iterator = parametros.iterator(); iterator.hasNext();) {
	*			String string = (String) iterator.next();
	*			consulta += " and" + string;
	*		}
	*		
	*		Query q = em.createQuery(consulta);
	*		if(consulta.contains("nome")) q.setParameter("nome", computador.getNome());
	*		if(consulta.contains("sistemaOperacional")) q.setParameter("sistemaOperacional", computador.getSistemaOperacional());
	*		if(consulta.contains("tipo")) q.setParameter("tipo", computador.getTipo());
	*		if(consulta.contains("dataCompra")) q.setParameter("dataCompra", computador.getEquipamento().getDataCompra());
	*		if(consulta.contains("garantia")) q.setParameter("garantia", computador.getEquipamento().getGarantia());
	*		if(consulta.contains("dataSubstituicao")) q.setParameter("dataSubstituicao", computador.getEquipamento().getDataSubstituicao());
	*		if(consulta.contains("marca")) q.setParameter("marca", computador.getEquipamento().getMarca());
	*		if(consulta.contains("modelo")) q.setParameter("modelo", computador.getEquipamento().getModelo());
	*		if(consulta.contains("patrimonio")) q.setParameter("patrimonio", computador.getEquipamento().getPatrimonio());
	*		if(consulta.contains("predio")) q.setParameter("predio", computador.getEquipamento().getPredio());
	*		if(consulta.contains("sala")) q.setParameter("sala", computador.getEquipamento().getSala());
	*		if(consulta.contains("serialNumber")) q.setParameter("serialNumber", computador.getEquipamento().getSerialNumber());
	*		
	*		
	*		return (List<Computador>) q.getResultList();
	*	} else return null;
	}*/
	
	/*@SuppressWarnings("unchecked")
	*public List<Computador> buscar(List<String> nomes, List<String> sistemasOperacionais, List<TipoComputador> tiposComputador, List<Date> datasCompra, 
	*		List<Date> datasSubstituicao, List<Date> garantias, List<String> marcas, List<String> modelos, List<Integer> patrimonios, List<String> predios, List<String> salas, List<String> seriais){
	*	
	*	String consulta = "select c from Computador c";
	*	List<String> parametros = new ArrayList<String>();
	*	
	*	parametros.addAll(montarParametros(nomes, sistemasOperacionais, tiposComputador));
	*	
	*	if(datasCompra.isEmpty() || datasSubstituicao.isEmpty() || garantias.isEmpty() || marcas.isEmpty() || modelos.isEmpty() || patrimonios.isEmpty() || predios.isEmpty() || salas.isEmpty()) {
	*		consulta += " JOIN Equipamento " + EquipamentoDao.PREFIXO + " where " + EquipamentoDao.PREFIXO + ".id = c.equipamento";
	*		parametros.addAll(EquipamentoDao.montarParametros(datasCompra, datasSubstituicao, garantias, marcas, modelos, patrimonios, predios, salas, seriais, null));
	*	}
	*	
	*	Query q = em.createQuery(consulta);
	*	if(consulta.contains("dataCompra")) q.setParameter("dataCompra", datasCompra);
	*	if(consulta.contains("garantia")) q.setParameter("garantia", garantias);
	*	if(consulta.contains("dataSubstituicao")) q.setParameter("dataSubstituicao", datasSubstituicao);
	*	if(consulta.contains("marca")) q.setParameter("marca", marcas);
	*	if(consulta.contains("modelo")) q.setParameter("modelo", modelos);
	*	if(consulta.contains("patrimonio")) q.setParameter("patrimonio", patrimonios);
	*	if(consulta.contains("predio")) q.setParameter("predio", predios);
	*	if(consulta.contains("sala")) q.setParameter("sala", salas);
	*	if(consulta.contains("serialNumber")) q.setParameter("serialNumber", seriais);
	*	
	*	return (List<Computador>) q.getResultList();
	}*/
	
	/*protected static List<String> montarParametros(Computador computador) {
	*	if(computador != null){
	*		boolean nome = computador.getNome() != null;
	*		boolean sistemaOperacional = computador.getSistemaOperacional() != null;
	*		boolean tipo = computador.getTipo() != null;
	*		
	*		ArrayList<String> parametros = new ArrayList<String>();
	*		
	*		if(nome) parametros.add(" c.nome like %:nome%");
	*		if(sistemaOperacional) parametros.add(" c.sistemaOperacional like %:sistemaOperacional%");
	*		if(tipo) parametros.add(" c.tipo = :tipo");
	*		
	*		return parametros;
	*	}
	*	else return null;
	}*/
	
	/*protected static List<String> montarParametros(List<String> nomes, List<String> sistemasOperacionais, List<TipoComputador> tipos) {
		List<String> parametros = new ArrayList<String>();
		
		if(!nomes.isEmpty()) parametros.add(" c.nome in (:nome)");
		if(!sistemasOperacionais.isEmpty()) parametros.add(PREFIXO + ".sistemaOperacional in (:sistemaOperacional)");
		if(!tipos.isEmpty()) parametros.add(" c.tipo in (:tipo)");
		
		return parametros;
	}*/
	
	

}
