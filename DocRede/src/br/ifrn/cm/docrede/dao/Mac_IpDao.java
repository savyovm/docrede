package br.ifrn.cm.docrede.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.ifrn.cm.docrede.dominio.Mac_Ip;

@Stateless
public class Mac_IpDao {
	
	@PersistenceContext
	private EntityManager em;
	
	public Mac_Ip salvarOuAtualizar(Mac_Ip macIp){
		if(macIp.getId() == 0) em.persist(macIp);
		else em.merge(macIp);
		
		return macIp;
	}
	
	public void remover(Mac_Ip macIp){
		macIp = em.find(Mac_Ip.class, macIp.getId());
		em.remove(macIp);
	}
	
	@SuppressWarnings("unchecked")
	public List<Mac_Ip> listar() {
		return (List<Mac_Ip>) em.createQuery("select mi from Mac_Ip mi").getResultList();
	}
	
	public Mac_Ip getMac_Ip(int id) {
		return em.find(Mac_Ip.class, id);
	}
}
