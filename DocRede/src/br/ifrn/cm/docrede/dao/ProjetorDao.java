package br.ifrn.cm.docrede.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.ifrn.cm.docrede.dominio.Equipamento;
import br.ifrn.cm.docrede.dominio.Projetor;

@Stateless
public class ProjetorDao {
	
	@PersistenceContext
	private EntityManager em;
	
	@Inject
	private EquipamentoDao equipamentoDao = new EquipamentoDao();
	
	public Projetor salvarOuAtualizar(Projetor projetor){
		projetor.setEquipamento(equipamentoDao.salvarOuAtualizar(projetor.getEquipamento()));
		if(projetor.getId() == 0) em.persist(projetor);
		else em.merge(projetor);
		
		return projetor;
	}
	
	public void remover(Projetor projetor){
		projetor = em.find(Projetor.class, projetor.getId());
		equipamentoDao.remover(projetor.getEquipamento());
		em.remove(projetor);
	}
	
	@SuppressWarnings("unchecked")
	public List<Projetor> listar() {
		return (List<Projetor>) em.createQuery("select pr from Projetor pr").getResultList();
	}
	
	public Projetor getProjetor(int id) {
		return em.find(Projetor.class, id);
	}
	
	public Projetor getByEquipamento(Equipamento id) {
		Query q = em.createQuery("SELECT pr FROM Projetor pr WHERE pr.equipamento = :equipamento");
		q.setParameter("equipamento", id);
		return (Projetor) q.getSingleResult();
	}
	
	/*protected static List<String> montarParametros(Projetor projetor) {
	*	if(projetor != null) {
	*		List<String> parametros = new ArrayList<String>();
	*		
	*		if(projetor.getNome() != null) parametros.add(" pr.nome like %:nome%");
	*		
	*		return parametros;
	*	} else return null;
	}*/
	
	/*protected static List<String> montarParametros(List<String> nomes) {
	*	List<String> parametros = new ArrayList<String>();
	*	
	*	if(!nomes.isEmpty()) parametros.add(" pr.nome in (:nome)");
	*	
	*	return parametros;
	}*/
}
