package br.ifrn.cm.docrede.dao;

import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.ifrn.cm.docrede.dominio.Equipamento;
import br.ifrn.cm.docrede.dominio.TipoEquipamento;

@Stateless
public class EquipamentoDao {
	
	@PersistenceContext
	private EntityManager em;
	
	public Equipamento salvarOuAtualizar(Equipamento equipamento){
		if(equipamento.getId() == 0) em.persist(equipamento);
		else em.merge(equipamento);
		
		return equipamento;
	}
	
	public void remover(Equipamento equipamento){		
		equipamento = em.find(Equipamento.class, equipamento.getId());
		em.remove(equipamento);
	}
	
	@SuppressWarnings("unchecked")
	public List<Equipamento> listar() {
		return (List<Equipamento>) em.createQuery("select e from Equipamento e").getResultList();
	}
	
	public Equipamento getEquipamento(int id) {
		return em.find(Equipamento.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Equipamento> listarOutros() {
		Query q = em.createQuery("SELECT e FROM Equipamento e WHERE e.tipo IN :tipos");
		q.setParameter("tipos", Arrays.asList(TipoEquipamento.ACESSPOINT, TipoEquipamento.CAMERA, TipoEquipamento.ESTABILIZADOR, TipoEquipamento.GATEWAY, TipoEquipamento.MONITOR, TipoEquipamento.NOBREAK, TipoEquipamento.OUTROS, TipoEquipamento.STORAGE));
		return (List<Equipamento>) q.getResultList();
		
	}

	public int contarEquipamentos(TipoEquipamento tipo) {
		Query q = em.createQuery("SELECT e FROM Equipamento e WHERE e.tipo = :tipo");
		q.setParameter("tipo", tipo);
		return q.getResultList().size();
	}
	
	/*protected static List<String> montarParametros(Equipamento equipamento){
	*	if(equipamento != null) {
	*		boolean compra = equipamento.getDataCompra() != null;		
	*		boolean substituicao = equipamento.getDataSubstituicao() != null;
	*		boolean garantia = equipamento.getGarantia() != null;
	*		boolean marca = equipamento.getMarca() != null;
	*		boolean modelo = equipamento.getModelo() != null;
	*		boolean patrimonio = equipamento.getPatrimonio() != 0;
	*		boolean predio = equipamento.getPredio() != null;
	*		boolean sala = equipamento.getSala() != null;
	*		boolean serial = equipamento.getSerialNumber() != null;
	*		boolean tipo = equipamento.getTipo() != null;
	*		
	*		ArrayList<String> parametros = new ArrayList<String>();
	*		
	*		if(compra) parametros.add(" e.dataCompra = :dataCompra");
	*		if(substituicao) parametros.add(" e.dataSusbstituicao = :dataSubstituicao");
	*		if(garantia) parametros.add(" e.garantia = :garantia");
	*		if(marca) parametros.add(" e.marca like %:marca%");
	*		if(modelo) parametros.add(" e.modelo like %:modelo%");
	*		if(patrimonio) parametros.add(" e.patrimonio = :patrimonio");
	*		if(predio) parametros.add(" e.predio like %:predio%");
	*		if(sala) parametros.add(" e.sala like %:sala%");
	*		if(serial) parametros.add(" e.serialNumber like %:serialNumber%");
	*		if(tipo) parametros.add(" e.tipo = :tipo");
	*		
	*		return parametros;
	*	}
	*	else return null;
	}*/
	
	/*protected static List<String> montarParametros(List<Date> datasCompra, List<Date> datasSubstituicao, List<Date> garantias, List<String> marcas, List<String> modelos, 
	*		List<Integer> patrimonios, List<String> predios, List<String> salas, List<String> seriais, List<TipoEquipamento> tipos){
	*	
	*	List<String> parametros = new ArrayList<String>();
	*	
	*	if(!datasCompra.isEmpty()) parametros.add(" e.dataCompra in (:dataCompra)");
	*	if(!datasSubstituicao.isEmpty()) parametros.add(" e.dataSubstituicao in (:dataSubstituicao)");
	*	if(!garantias.isEmpty()) parametros.add(" e.garantia in (:garantia)");
	*	if(!marcas.isEmpty()) parametros.add(" e.marca in (:marca)");
	*	if(!modelos.isEmpty()) parametros.add(" e.modelo in (:modelo)");
	*	if(!patrimonios.isEmpty()) parametros.add(" e.patrimonio in (:patrimonio)");
	*	if(!predios.isEmpty()) parametros.add(" e.predio in (:predio)");
	*	if(!salas.isEmpty()) parametros.add(" e.sala in (:sala)");
	*	if(!seriais.isEmpty()) parametros.add(" e.sala in (:serialNumber)");
	*	if(!tipos.isEmpty()) parametros.add(" e.tipo in (:tipo)");
	*	
	*	return parametros;
	}*/
}
