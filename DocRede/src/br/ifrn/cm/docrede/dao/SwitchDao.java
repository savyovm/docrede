package br.ifrn.cm.docrede.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.ifrn.cm.docrede.dominio.Equipamento;
import br.ifrn.cm.docrede.dominio.Switch;

@Stateless
public class SwitchDao {
	protected static String PREFIXO = "sw";
	
	@PersistenceContext
	private EntityManager em;
	
	@Inject
	private EquipamentoDao equipamentoDao = new EquipamentoDao();
	
	public Switch salvarOuAtualizar(Switch sw){
		sw.setEquipamento(equipamentoDao.salvarOuAtualizar(sw.getEquipamento()));
		if(sw.getId() == 0) em.persist(sw);
		else em.merge(sw);
		
		return sw;
	}
	
	public void remover(Switch sw){
		sw = em.find(Switch.class, sw.getId());
		equipamentoDao.remover(sw.getEquipamento());
		em.remove(sw);
	}
	
	@SuppressWarnings("unchecked")
	public List<Switch> listar() {
		return (List<Switch>) em.createQuery("select sw from Switch sw").getResultList();
	}
	
	public Switch getSwitch(int id) {
		return em.find(Switch.class, id);
	}
	
	public Switch getByEquipamento(Equipamento id) {
		Query q = em.createQuery("SELECT sw FROM Switch sw WHERE sw.equipamento = :equipamento");
		q.setParameter("equipamento", id);
		return (Switch) q.getSingleResult();
	}
	
	/*protected static List<String> montarParametros(Switch sw) {
	*	if(sw != null){
	*		List<String> parametros = new ArrayList<String>();
	*		
	*		if(sw.getPortas() != 0) parametros.add(" sw.portas = :portas");
	*		if(sw.getRack() != 0) parametros.add(" sw.rack = :rack");
	*		
	*		return parametros;
	*	} else return null;
	}*/
	
	/*protected static List<String> montarParametros(List<Integer> portas, List<Integer> racks) {
	*	List<String> parametros = new ArrayList<String>();
	*	
	*	if(!portas.isEmpty()) parametros.add(" sw.portas in (:portas)");
	*	if(!racks.isEmpty()) parametros.add(" sw.rack in (:rack)");
	*	
	*	return parametros;
	}*/
}
