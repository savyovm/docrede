package br.ifrn.cm.docrede.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.ifrn.cm.docrede.dominio.Computador;
import br.ifrn.cm.docrede.dominio.Servidor;

@Stateless
public class ServidorDao {
	
	@PersistenceContext
	private EntityManager em;
	
	@Inject
	private ComputadorDao computadorDao = new ComputadorDao();
	
	public Servidor salvarOuAtualizar(Servidor servidor){
		servidor.setComputador(computadorDao.salvarOuAtualizar(servidor.getComputador()));
		if(servidor.getId() == 0) em.persist(servidor);
		else em.merge(servidor);
		
		return servidor;
	}
	
	public void remover(Servidor servidor){
		servidor = em.find(Servidor.class, servidor.getId());
		computadorDao.remover(servidor.getComputador());
		em.remove(servidor);
	}
	
	@SuppressWarnings("unchecked")
	public List<Servidor> listar() {
		return (List<Servidor>) em.createQuery("select sr from Servidor sr").getResultList();
	}
	
	public Servidor getServidor(int id) {
		return em.find(Servidor.class, id);
	}
	
	public Servidor getByComputador(Computador computador) {
		Query q = em.createQuery("select sr FROM Servidor sr WHERE sr.computador = :computador");
		q.setParameter("computador", computador);
		return (Servidor) q.getSingleResult();
	}
}
