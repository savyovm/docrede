package br.ifrn.cm.docrede.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.ifrn.cm.docrede.dominio.Grupo;

@Stateless
public class GrupoDao {
	
	@PersistenceContext
	private EntityManager em;
	
	public Grupo salvarOuAtualizar(Grupo grupo){
		if(grupo.getId() == 0) em.persist(grupo);
		else em.merge(grupo);
		
		return grupo;
	}
	
	public void remover(Grupo grupo){
		grupo = em.find(Grupo.class, grupo.getId());
		em.remove(grupo);
	}
	
	@SuppressWarnings("unchecked")
	public List<Grupo> listar() {
		return (List<Grupo>) em.createQuery("select g from Grupo g").getResultList();
	}
	
	public Grupo getGrupo(int id) {
		return em.find(Grupo.class, id);
	}
	
	/*protected static List<String> montarParametros(Grupo grupo){
	*	
	*	if(grupo != null){
	*		List<String> parametros = new ArrayList<String>();
	*		
	*		if(grupo.getNome() != null) parametros.add(" g.nome like %:nome%");
	*		if(grupo.getCota() >= 0) parametros.add(" g.cota = :cota");
	*		
	*		return parametros;
	*	} else return null;
	}*/
	
	/*protected static List<String> montarParametros(List<String> nomes, List<Integer> cotas) {
	*	List<String> parametros = new ArrayList<String>();
	*	
	*	if(!nomes.isEmpty()) parametros.add(" g.nome in (:nome)");
	*	if(!cotas.isEmpty()) parametros.add(" g.cota in (:cota)");
	*	
	*	return parametros;
	}*/

}
