package br.ifrn.cm.docrede.dao;

import java.util.Hashtable;

import javax.ejb.Stateless;
import javax.naming.CommunicationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.*;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;

@Stateless
public class UsuarioDao {

	private final static String CONTEXT_FACTORY_CLASS = "com.sun.jndi.ldap.LdapCtxFactory";

	private final static String domainName = "IFRN.LOCAL";

	public boolean autenticar(String usuario, String senha) throws LoginException {
		if (usuario == null || senha == null || usuario.trim().length() == 0 || senha.trim().length() == 0) {
			throw new FailedLoginException("Username or password is empty");
		}
		
		try {
			Hashtable<Object, Object> env = new Hashtable<Object, Object>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, CONTEXT_FACTORY_CLASS);
			env.put(Context.PROVIDER_URL, "ldap://10.62.0.155:389");
			env.put(Context.SECURITY_PRINCIPAL, usuario + "@" + domainName);
			env.put(Context.SECURITY_CREDENTIALS, senha);
			env.put("java.naming.ldap.attributes.binary","tokenGroups"); //NOVA LINHA
			DirContext ctx = new InitialDirContext(env);
				
//*******************************************NOVO C�DIGO*******************************************
			//Note the userdn here does not have the username - we have already binded to active directory.
			//You'll need to change this string for your domain/structure.
			String userdn = "DC=ifrn,DC=local";
			SearchControls searchCtrls = new SearchControls();
			searchCtrls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String[] atributos = {"member","memberof"};
			searchCtrls.setReturningAttributes(atributos);
			
			//Change the NameOfGroup for the group name you would like to retrieve the members of.
			String filtro = "(&(objectCategory=group)(name=G_CTI_CM))";
			
			//use the context we created above and the filter to return all members of a group.
			NamingEnumeration<SearchResult> valores = ctx.search( userdn, filtro, searchCtrls);
			
			//Loop through the search results
			while (valores.hasMoreElements()) {
				SearchResult sr = (SearchResult)valores.next();
				Attributes attrs = sr.getAttributes();
				
				if (null != attrs) {
					for (NamingEnumeration<? extends Attribute> ae = attrs.getAll(); ae.hasMoreElements();) {
						Attribute atr = (Attribute) ae.next();
						NamingEnumeration<?> vals = atr.getAll();					
						
						while(vals.hasMoreElements()) {
							String un = (String) vals.nextElement();
							if(usuario.equals(un.split(",")[0].substring(3))) {
								ctx.close();
								return true;
							}
						}
					}
				}
			}
//*****************************************FIM NOVO C�DIGO*****************************************
			
			ctx.close();
			return false;
		} catch (CommunicationException exp) {
			exp.printStackTrace();
			return false;
		} catch (Throwable throwable) {
			throwable.printStackTrace();
			return false;
		}
	}
}