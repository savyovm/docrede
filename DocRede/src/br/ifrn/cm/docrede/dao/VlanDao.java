package br.ifrn.cm.docrede.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.ifrn.cm.docrede.dominio.Vlan;

@Stateless
public class VlanDao {
	
	@PersistenceContext
	private EntityManager em;
	
	public Vlan salvarOuAtualizar(Vlan vlan){
		if(vlan.getId() == 0) em.persist(vlan);
		else em.merge(vlan);
		
		return vlan;
	}
	
	public void remover(Vlan vlan){
		vlan = em.find(Vlan.class, vlan.getId());
		em.remove(vlan);
	}
	
	@SuppressWarnings("unchecked")
	public List<Vlan> listar() {
		return (List<Vlan>) em.createQuery("select v from Vlan v").getResultList();
	}
	
	public Vlan getVlan(int id) {
		return em.find(Vlan.class, id);
	}
	
	/*protected static List<String> montarParametros(Vlan vlan){
	*	if(vlan != null) {
	*		List<String> parametros = new ArrayList<String>();
	*		
	*		if(vlan.getDescricao() != null) parametros.add(" v.descricao like %:descricao%");
	*		if(vlan.getNumero() != 0) parametros.add(" v.numero = :numero");
	*		
	*		return parametros;
	*	} else return null;
	}*/
	
	/*protected static List<String> montarParametros(List<String> descricoes, List<Integer> numeros) {
	*	List<String> parametros = new ArrayList<String>();
	*	
	*	if(!descricoes.isEmpty()) parametros.add(" v.descricao in (:descricao)");
	*	if(!numeros.isEmpty()) parametros.add(" v.numero in (:numero)");
	*	
	*	return parametros;
	}*/
}
