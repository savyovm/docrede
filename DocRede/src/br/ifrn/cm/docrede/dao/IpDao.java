package br.ifrn.cm.docrede.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.ifrn.cm.docrede.dominio.Ip;

@Stateless
public class IpDao {
	
	@PersistenceContext
	private EntityManager em;
	
	public Ip salvarOuAtualizar(Ip ip){
		if(ip.getId() == 0) em.persist(ip);
		else em.merge(ip);
		
		return ip;
	}
	
	public void remover(Ip ip){
		ip = em.find(Ip.class, ip.getId());
		em.remove(ip);
	}
	
	@SuppressWarnings("unchecked")
	public List<Ip> listar() {
		return (List<Ip>) em.createQuery("select ip from Ip ip").getResultList();
	}
	
	public Ip getIp(int id) {
		return em.find(Ip.class, id);
	}
	
	/*protected static List<String> montarParametros(Ip ip){
	*	if(ip != null){
	*		List<String> parametros = new ArrayList<String>();
	*		
	*		if(ip.getEndereco() != null) parametros.add(" ip.endereco like %:endereco%");
	*		if(ip.getMascara() != null) parametros.add(" ip.mascara like %:mascara%");
	*		
	*		return parametros;
	*	} else return null;
	}*/
	
	/*protected static List<String> montarParametros(List<String> enderecos, List<String> mascaras){
	*	List<String> parametros = new ArrayList<String>();
	*	
	*	if(!enderecos.isEmpty()) parametros.add(" ip.endereco in (:endereco)");
	*	if(!mascaras.isEmpty()) parametros.add(" ip.mascara in (:mascara)");
	*	
	*	return parametros;
	}*/
}
