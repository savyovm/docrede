package br.ifrn.cm.docrede.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.ifrn.cm.docrede.dominio.Permissao;

@Stateless
public class PermissaoDao {
	
	@PersistenceContext
	private EntityManager em;
	
	public Permissao salvarOuAtualizar(Permissao permissao){
		if(permissao.getId() == 0) em.persist(permissao);
		else em.merge(permissao);
		
		return permissao;
	}
	
	public void remover(Permissao permissao){
		permissao = em.find(Permissao.class, permissao.getId());
		em.remove(permissao);
	}
	
	@SuppressWarnings("unchecked")
	public List<Permissao> listar() {
		return (List<Permissao>) em.createQuery("select p from Permissao p").getResultList();
	}
	
	public Permissao getPermissao(int id) {
		return em.find(Permissao.class, id);
	}
	
	
}
