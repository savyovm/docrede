package br.ifrn.cm.docrede.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.ifrn.cm.docrede.dominio.Mac;

@Stateless
public class MacDao {
	
	@PersistenceContext
	private EntityManager em;
	
	public Mac salvarOuAtualizar(Mac mac){
		if(mac.getId() == 0) em.persist(mac);
		else em.merge(mac);
		
		return mac;
	}
	
	public void remover(Mac mac){
		mac = em.find(Mac.class, mac.getId());
		em.remove(mac);
	}
	
	@SuppressWarnings("unchecked")
	public List<Mac> listar() {
		return (List<Mac>) em.createQuery("select m from Mac m").getResultList();
	}
	
	public Mac getMac(int id) {
		return em.find(Mac.class, id);
	}
	
	/*protected static List<String> montarParametros(Mac mac) {
	*	if(mac != null) {
	*		List<String> parametros = new ArrayList<String>();
	*		
	*		if(mac.getEndereco() != null) parametros.add(" m.endereco like %:endereco%");
	*		if(mac.getPorta() != null) parametros.add(" m.porta like %:porta%");
	*		
	*		return parametros;
	*	} else return null;
	}*/
	
	/*protected static List<String> montarParametros(List<String> enderecos, List<String> portas) {
	*	List<String> parametros = new ArrayList<String>();
	*	
	*	if(!enderecos.isEmpty()) parametros.add(" m.endereco in (:endereco)");
	*	if(!portas.isEmpty()) parametros.add(" m.porta in (:porta)");
	*	
	*	return parametros;
	}*/
}
