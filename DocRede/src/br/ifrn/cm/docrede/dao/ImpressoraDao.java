package br.ifrn.cm.docrede.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.ifrn.cm.docrede.dominio.Equipamento;
import br.ifrn.cm.docrede.dominio.Impressora;

@Stateless
public class ImpressoraDao {
	
	@PersistenceContext
	private EntityManager em;
	
	@Inject
	private EquipamentoDao equipamentoDao = new EquipamentoDao();
	
	public Impressora salvarOuAtualizar(Impressora impressora){
		impressora.setEquipamento(equipamentoDao.salvarOuAtualizar(impressora.getEquipamento()));
		if(impressora.getId() == 0) em.persist(impressora);
		else em.merge(impressora);
		
		return impressora;
	}
	
	public void remover(Impressora impressora){
		impressora = em.find(Impressora.class, impressora.getId());
		equipamentoDao.remover(impressora.getEquipamento());
		em.remove(impressora);
	}
	
	@SuppressWarnings("unchecked")
	public List<Impressora> listar() {
		return (List<Impressora>) em.createQuery("select i from Impressora i").getResultList();
	}
	
	public Impressora getImpressora(int id) {
		return em.find(Impressora.class, id);
	}

	public Impressora getByEquipamento(Equipamento id) {
		Query q = em.createQuery("SELECT i FROM Impressora i WHERE i.equipamento = :equipamento");
		q.setParameter("equipamento", id);
		return (Impressora) q.getSingleResult();
	}

	/*protected static List<String> montarParametros(Impressora impressora) {
	*	if(impressora != null) {
	*		List<String> parametros = new ArrayList<String>();
	*		
	*		if(impressora.getNome() != null) parametros.add(" i.nome like %:nome%");
	*		
	*		return parametros;
	*	} else return null;
	}*/
	
	/*protected static List<String> montarParametros(List<String> nomes){
	*	List<String> parametros = new ArrayList<String>();
	*	
	*	if(!nomes.isEmpty()) parametros.add(" i.nome in (:nome)");
	*	
	*	return parametros;
	}*/
}
