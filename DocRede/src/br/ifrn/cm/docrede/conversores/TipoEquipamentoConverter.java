package br.ifrn.cm.docrede.conversores;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.ifrn.cm.docrede.dominio.TipoEquipamento;

@FacesConverter("tipoEquipamentoConverter")
public class TipoEquipamentoConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		if(arg2 != null) {
			return TipoEquipamento.valueOf(arg2);
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if(arg2 != null && !"".equals(arg2)) {
			return arg2.toString();
		}
		return null;
	}
	
}
