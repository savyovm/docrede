package br.ifrn.cm.docrede.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import br.ifrn.cm.docrede.dao.Mac_IpDao;
import br.ifrn.cm.docrede.dominio.Mac_Ip;

@Stateless
public class Mac_IpService {
	
	@Inject
	private Mac_IpDao dao = new Mac_IpDao();
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Mac_Ip salvarOuAtualizar(Mac_Ip macIp){
		return dao.salvarOuAtualizar(macIp);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(Mac_Ip macIp){
		dao.remover(macIp);
	}
	
	public List<Mac_Ip> listar() {
		return dao.listar();
	}
	
	public Mac_Ip getMac_Ip(int id) {
		return dao.getMac_Ip(id);
	}
}
