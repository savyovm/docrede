package br.ifrn.cm.docrede.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import br.ifrn.cm.docrede.dao.ComputadorDao;
import br.ifrn.cm.docrede.dominio.Computador;
import br.ifrn.cm.docrede.dominio.Equipamento;

@Stateless
public class ComputadorService {
	
	@Inject
	private ComputadorDao dao = new ComputadorDao();
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Computador salvarOuAtualizar(Computador computador){
		return dao.salvarOuAtualizar(computador);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(Computador computador){
		dao.remover(computador);
	}
	
	public List<Computador> listar() {
		return dao.listar();
	}
	
	public Computador getComputador(int id) {
		return dao.getComputador(id);
	}
	
	public Computador getByEquipamento(Equipamento id) {
		return dao.getByEquipamento(id);
	}
}
