package br.ifrn.cm.docrede.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import br.ifrn.cm.docrede.dao.VlanDao;
import br.ifrn.cm.docrede.dominio.Vlan;

@Stateless
public class VlanService {
	
	@Inject
	private VlanDao dao = new VlanDao();
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Vlan salvarOuAtualizar(Vlan vlan){
		return dao.salvarOuAtualizar(vlan);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(Vlan vlan){
		dao.remover(vlan);
	}
	
	public List<Vlan> listar() {
		return dao.listar();
	}
	
	public Vlan getVlan(int id) {
		return dao.getVlan(id);
	}
}
