package br.ifrn.cm.docrede.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import br.ifrn.cm.docrede.dao.RamalDao;
import br.ifrn.cm.docrede.dominio.Equipamento;
import br.ifrn.cm.docrede.dominio.Ramal;

@Stateless
public class RamalService {
	
	@Inject
	private RamalDao dao = new RamalDao();
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Ramal salvarOuAtualizar(Ramal ramal){
		return dao.salvarOuAtualizar(ramal);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(Ramal ramal){
		dao.remover(ramal);
	}
	
	public List<Ramal> listar() {
		return dao.listar();
	}
	
	public Ramal getRamal(int id) {
		return dao.getRamal(id);
	}
	
	public Ramal getByEquipamento(Equipamento id) {
		return dao.getByEquipamento(id);
	}
}
