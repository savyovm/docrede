package br.ifrn.cm.docrede.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import br.ifrn.cm.docrede.dao.MacDao;
import br.ifrn.cm.docrede.dominio.Mac;

@Stateless
public class MacService {
	
	@Inject
	private MacDao dao = new MacDao();
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Mac salvarOuAtualizar(Mac mac){
		return dao.salvarOuAtualizar(mac);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(Mac mac){
		dao.remover(mac);
	}
	
	public List<Mac> listar() {
		return dao.listar();
	}
	
	public Mac getMac(int id) {
		return dao.getMac(id);
	}
}
