package br.ifrn.cm.docrede.negocio;

import javax.ejb.Stateless;
import javax.security.auth.login.LoginException;

import br.ifrn.cm.docrede.dao.UsuarioDao;

@Stateless
public class UsuarioService {
	private UsuarioDao usr = new UsuarioDao();
	
	public boolean autenticar(String usuario, String senha) {
		try {
			return usr.autenticar(usuario, senha);
		} catch (LoginException e) {
			return false;
		}
	}
}
