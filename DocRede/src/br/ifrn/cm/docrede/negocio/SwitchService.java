package br.ifrn.cm.docrede.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import br.ifrn.cm.docrede.dao.SwitchDao;
import br.ifrn.cm.docrede.dominio.Equipamento;
import br.ifrn.cm.docrede.dominio.Switch;

@Stateless
public class SwitchService {
	
	@Inject
	private SwitchDao dao = new SwitchDao();
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Switch salvarOuAtualizar(Switch sw){
		return dao.salvarOuAtualizar(sw);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(Switch sw){
		dao.remover(sw);
	}
	
	public List<Switch> listar() {
		return dao.listar();
	}
	
	public Switch getSwitch(int id) {
		return dao.getSwitch(id);
	}
	
	public Switch getByEquipamento(Equipamento id) {
		return dao.getByEquipamento(id);
	}
}
