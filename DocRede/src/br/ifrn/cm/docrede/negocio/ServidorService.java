package br.ifrn.cm.docrede.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import br.ifrn.cm.docrede.dao.ServidorDao;
import br.ifrn.cm.docrede.dominio.Computador;
import br.ifrn.cm.docrede.dominio.Servidor;

@Stateless
public class ServidorService {
	
	@Inject
	private ServidorDao dao = new ServidorDao();
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Servidor salvarOuAtualizar(Servidor servidor){
		return dao.salvarOuAtualizar(servidor);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(Servidor servidor){
		dao.remover(servidor);
	}
	
	public List<Servidor> listar() {
		return dao.listar();
	}
	
	public Servidor getServidor(int id) {
		return dao.getServidor(id);
	}
	
	public Servidor getByComputador(Computador computador) {
		return dao.getByComputador(computador);
	}
}
