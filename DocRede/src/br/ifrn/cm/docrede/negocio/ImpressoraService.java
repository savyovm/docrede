package br.ifrn.cm.docrede.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import br.ifrn.cm.docrede.dao.ImpressoraDao;
import br.ifrn.cm.docrede.dominio.Equipamento;
import br.ifrn.cm.docrede.dominio.Impressora;

@Stateless
public class ImpressoraService {
	
	@Inject
	private ImpressoraDao dao = new ImpressoraDao();
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Impressora salvarOuAtualizar(Impressora impressora){
		return dao.salvarOuAtualizar(impressora);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(Impressora impressora){
		dao.remover(impressora);
	}
	
	public List<Impressora> listar() {
		return dao.listar();
	}
	
	public Impressora getImpressora(int id) {
		return dao.getImpressora(id);
	}
	
	public Impressora getByEquipamento(Equipamento id) {
		return dao.getByEquipamento(id);
	}
}
