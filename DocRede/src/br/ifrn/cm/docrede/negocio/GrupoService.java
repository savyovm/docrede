package br.ifrn.cm.docrede.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import br.ifrn.cm.docrede.dao.GrupoDao;
import br.ifrn.cm.docrede.dominio.Grupo;

@Stateless
public class GrupoService {
	
	@Inject
	private GrupoDao dao = new GrupoDao();
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Grupo salvarOuAtualizar(Grupo grupo){
		return dao.salvarOuAtualizar(grupo);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(Grupo grupo){
		dao.remover(grupo);
	}
	
	public List<Grupo> listar() {
		return dao.listar();
	}
	
	public Grupo getGrupo(int id) {
		return dao.getGrupo(id);
	}
}
