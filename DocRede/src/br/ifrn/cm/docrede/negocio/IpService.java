package br.ifrn.cm.docrede.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import br.ifrn.cm.docrede.dao.IpDao;
import br.ifrn.cm.docrede.dominio.Ip;

@Stateless
public class IpService {
	
	@Inject
	private IpDao dao = new IpDao();
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Ip salvarOuAtualizar(Ip ip){
		return dao.salvarOuAtualizar(ip);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(Ip ip){
		dao.remover(ip);
	}
	
	public List<Ip> listar() {
		return dao.listar();
	}
	
	public Ip getIp(int id) {
		return dao.getIp(id);
	}
}
