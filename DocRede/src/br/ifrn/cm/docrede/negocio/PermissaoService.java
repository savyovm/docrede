package br.ifrn.cm.docrede.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import br.ifrn.cm.docrede.dao.PermissaoDao;
import br.ifrn.cm.docrede.dominio.Permissao;

@Stateless
public class PermissaoService {
	
	@Inject
	private PermissaoDao dao = new PermissaoDao();
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Permissao salvarOuAtualizar(Permissao permissao){
		return dao.salvarOuAtualizar(permissao);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(Permissao permissao){
		dao.remover(permissao);
	}
	
	public List<Permissao> listar() {
		return dao.listar();
	}
	
	public Permissao getPermissao(int id) {
		return dao.getPermissao(id);
	}
}
