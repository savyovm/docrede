package br.ifrn.cm.docrede.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import br.ifrn.cm.docrede.dao.EquipamentoDao;
import br.ifrn.cm.docrede.dominio.Equipamento;
import br.ifrn.cm.docrede.dominio.TipoEquipamento;

@Stateless
public class EquipamentoService {
	
	@Inject
	private EquipamentoDao dao = new EquipamentoDao();
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Equipamento salvarOuAtualizar(Equipamento equipamento){
		return dao.salvarOuAtualizar(equipamento);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(Equipamento equipamento){
		dao.remover(equipamento);
	}
	
	public List<Equipamento> listar() {
		return dao.listar();
	}
	
	public Equipamento getEquipamento(int id) {
		return dao.getEquipamento(id);
	}
	
	public List<Equipamento> listarOutros() {
		return dao.listarOutros();
	}

	public int contarEquipamentos(TipoEquipamento tipo) {
		return dao.contarEquipamentos(tipo);
	}
}
