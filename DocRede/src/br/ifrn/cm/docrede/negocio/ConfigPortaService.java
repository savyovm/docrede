package br.ifrn.cm.docrede.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import br.ifrn.cm.docrede.dao.ConfigPortaDao;
import br.ifrn.cm.docrede.dominio.ConfigPorta;

@Stateless
public class ConfigPortaService {
	
	@Inject
	private ConfigPortaDao dao = new ConfigPortaDao();
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ConfigPorta salvarOuAtualizar(ConfigPorta configPorta){
		return dao.salvarOuAtualizar(configPorta);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(ConfigPorta configPorta){
		dao.remover(configPorta);
	}
	
	public List<ConfigPorta> listar() {
		return dao.listar();
	}
	
	public ConfigPorta getConfigPorta(int id) {
		return dao.getConfigPorta(id);
	}
}
