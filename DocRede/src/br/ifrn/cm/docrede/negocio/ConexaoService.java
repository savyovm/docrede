package br.ifrn.cm.docrede.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import br.ifrn.cm.docrede.dao.ConexaoDao;
import br.ifrn.cm.docrede.dominio.Conexao;

@Stateless
public class ConexaoService {
	
	@Inject
	private ConexaoDao dao = new ConexaoDao();
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Conexao salvarOuAtualizar(Conexao conexao){
		return dao.salvarOuAtualizar(conexao);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(Conexao conexao){
		dao.remover(conexao);
	}
	
	public List<Conexao> listar() {
		return dao.listar();
	}
	
	public Conexao getConexao(int id) {
		return dao.getConexao(id);
	}
}
