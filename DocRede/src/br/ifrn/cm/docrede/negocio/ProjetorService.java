package br.ifrn.cm.docrede.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import br.ifrn.cm.docrede.dao.ProjetorDao;
import br.ifrn.cm.docrede.dominio.Equipamento;
import br.ifrn.cm.docrede.dominio.Projetor;

@Stateless
public class ProjetorService {
	
	@Inject
	private ProjetorDao dao = new ProjetorDao();
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Projetor salvarOuAtualizar(Projetor projetor){
		return dao.salvarOuAtualizar(projetor);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remover(Projetor projetor){
		dao.remover(projetor);
	}
	
	public List<Projetor> listar() {
		return dao.listar();
	}
	
	public Projetor getProjetor(int id) {
		return dao.getProjetor(id);
	}
	
	public Projetor getByEquipamento(Equipamento id) {
		return dao.getByEquipamento(id);
	}
}
